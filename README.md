
# Projekt  MIT
## Teploměr LM35 s LCD displejem
Jak je obvod zapojený?

K mikroprocesoru STM8 jsme připojili Teploměr, potenciometr a Led displej. Teploměr je připojen na STM8 na vstup PB2. Potenciometr slouží k nastavení kontrastu displeje. Potenciometr je zapojen na vstup 3 na displeji.

Slovní popis funkce

Program se skládá z několika částí, nejdříve si vyvoláme funkce z importovaných knihoven, konkrétně lcd displey a milis. Milis slouží k počítání milisekund od startu programu, nasledně ho využijeme na obnovení měření teploty, konkretně aby teploměr posílal hodnotu každých 300milisekund. V další funkci ADC_init inicializujeme ADC převodník, který potřebujeme k převedení analogové hodnoty, kterou dostáváme od teploměru na digitální s kterou už dokážeme dále pracovat. Následně se spustí neustále se opakující smyčka, kde se do proměnné adc_value zapisuje hodnota z teploměru na pinu PB2. Ta se převádí na hodnotu reálné teploty a ukládá se do proměnné teplota. Na displeji se na souřadnicích 0, 0 zapíše teplota ve stupních celsia. 

## Blokové schéma
```mermaid
flowchart LR

    STM8-->LCD 
    LM35-->STM8
    PC-->STM8
    

```

## Tabulka součástek
|   Typ součástky |      Počet kusů      |  Cena/1 ks  | Cena  |
|:---------------:|:--------------------:|:-----------:|:-----:|
|   LCD Displej   |    1                 | 120 Kč      | 120 Kč|
|   Potenciometr  |    1                 |  15 Kč      | 15 Kč |
| STM8 Nucleo     |    1                 | 250 Kč      | 250 Kč|
|     LM35        |    1                 |  80 Kč      | 80 Kč |

## Schéma v KiCadu

![Schéma v KiCadu](MIT_schema.png)

## Fotka výtvoru

![Fotka vytvoru](vytvor.jpeg "Vytvor")
![Fotka teplomeru](teplomer.jpeg "Teplomer")

## Závěr

Vzhledem k tomu, že mikroprocesorová technika a programování celkově nejsou moji velcí kamarádi, tak to pro mě bylo docela utrpení, ale naučil jsem se nové věci, například používat LCD displej, ADC převodník a například i používat git.








