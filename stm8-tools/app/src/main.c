#include "stm8s.h"
#include "milis.h"
#include "stm8_hd44780.h"
#include "spse_stm8.h"

void setup(void)
{
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); //16MHz
    lcd_init();
    init_milis();

}

void lcd_print(unsigned char x_pos, unsigned char y_pos, unsigned int value)
{
    char tmp[5] = {0x20, 0x20, 0x20, 0x20, '\0'} ; //vytvoření seznamu
    tmp[0] = (((value / 100) % 10) + 0x30); //desítky
    tmp[1] = (((value / 10) % 10) + 0x30); //jednotky
    tmp[2] = 44; //desetinná čárka
    tmp[3] = ((value % 10) + 0x30); //desetinna cast
    lcd_gotoxy(x_pos,y_pos);
    lcd_puts(tmp);
}

void ADC_init(void)
{
ADC2_SchmittTriggerConfig(ADC2_SCHMITTTRIG_CHANNEL2,DISABLE);
ADC2_PrescalerConfig(ADC2_PRESSEL_FCPU_D4);
ADC2_AlignConfig(ADC2_ALIGN_RIGHT);
ADC2_Select_Channel(ADC2_CHANNEL_2);
ADC2_Cmd(ENABLE);
ADC2_Startup_Wait();
}

int main(void)
{
    uint32_t time = 0;
    uint16_t adc_value;
    int16_t teplota;

    setup();
    ADC_init();

    while (1){
        if (milis()-time>300){
            time=milis();
               
        adc_value = ADC_get(ADC2_CHANNEL_2); // do adc_value ulož výsledek převodu vstupu ADC_IN2 (PB2)
        teplota = adc_value*3;

        lcd_print(0,0,teplota);
        lcd_gotoxy(4,0);
        lcd_puts(" C");


    }
    }

}
